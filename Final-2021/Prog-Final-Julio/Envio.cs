﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prog_Final_Julio
{
    public class Envio
    {
        public int EnvioAutoNumerico { get; set; }
        public int DNIDesitnatario { get; set; }
        public int DNIRepartidor { get; set; }
        public tipo EstadoEnvio { get; set; }
        public enum tipo
        {
            PENDIENTE_ENVIO,
            ENVIADO,
            ULTIMO_TRAMO_DEL_RECORRIDO,
            ENTREGADO

        }
        public DateTime FechaEstimadaEntrega { get; set; }
        public DateTime FechaEntrega { get; set; }
        public double PesoenKG { get; set; }
        public List<EventosAsociados> EventosAsociados { get; set; }
        public bool EnvioProgramado { get; set; }
        public string DescripcionPaquete { get; set; }

        //CORRECCIÓN: ESTO NO FUNCIONA, SIEMPRE SERÁ UNO, REPASAR EJERCICIOS REALIZADOS!!
        public int GenerarCodigoAutoincremental()
        {

            return EnvioAutoNumerico += 1;
        }

    }
}
