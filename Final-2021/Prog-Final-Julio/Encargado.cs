﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prog_Final_Julio
{
    public class Encargado: Persona
    {
        public List<int> ListadoCodigoPostales { get; set; }
        public double CapacidadMaximaEnKG { get; set; }
        //CORRECCIÓN: ESTE LISTADO ESTÁ DEMÁS
        public List<String> LocalidadesPermitidas { get; set; }

    }
}
