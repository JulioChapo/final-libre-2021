﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prog_Final_Julio
{
    public class Persona
    {
        public int DNI { get; set; }
        public string NombreYApellido { get; set; }
        //CORRECCIÓN: EL CODIGO POSTAL NO SE NECESITA PARA EL ENCARGADO, DE HECHO EL ENCARGADO TIENE UN LISTADO.
        public int CodigoPostal { get; set; }
        public int TelContacto { get; set; }
    }
}
